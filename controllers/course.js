const Course = require("../models/Course");
const auth = require("../auth");

// Controller function for creating a course
module.exports.addCourse = (data) => {
	// Create variable "newCourse" and instantiates a new "Course" object
	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});

	// User is not an admin
	} else {
		return Promise.resolve(false);
	};
};


module.exports.getAllCourses =() => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Controller function for getting specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Controller function for updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
	// Specify the fields or properties of documents to be updated
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, err) => {
		if(err){
			return false;
		} else {
			return true;
		}
	})
}

// Controller function for achiving a course
module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
		// Course not archived
		if (error) {
			return false;
		// Course archived successfully
		} else {
			return true;

		}
	});
};

