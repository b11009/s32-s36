// imports the User model
const User = require("../models/User");
// imports the Course model
const Course = require("../models/Course");

const bcrypt = require("bcrypt");
const auth = require("../auth");


// Controller function for checking email duplicates
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {
		if(result.length > 0){
			return true
		} else {
			return false
		}
	})
}

// Controller function for user registration
module.exports.registerUser = (reqBody) => {
	// Creates variable "newUser" and instantiates a new "User" object using the mongoose model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		isAdmin : reqBody.isAdmin,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Save the created object to our database
	return newUser.save().then((user, error) => {
		// Failed registration
		if(error){
			return false;
		}
		// Successful User Registration
		else {
			return true;
		}
	})
}

// Create controller for user authentication(/login)
// 1st step check if email exists, then password
module.exports.loginUser= (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// if user doesnt exist
		if (result == null){
			return false;
		}
		// if user exists
		else {
			// compareSync() - used to compare not encrypted password to encrypted from database
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				// if true , Generate an access token
				return {access : auth.createAccessToken(result)}
			}
			// if not true, password do not match
			else{
				return false;
			}
		}
	})
}

// Create a controller for retrieving the details of the user
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then((result) => {
		
		// Changes the value of the user's password to an empty string when returned to the frontend
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};


// Controller for enrolling the user to a specific course
module.exports.enroll = async (data) => {
	// Add the course ID in the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the courseId in the user's enrollments array
		user.enrollments.push({courseId : data.courseId});

		// Save updated user information in the database
		return user.save().then((user, err) => {
			if(err){
				return false;
			}else{
				return true;
			}
		})
	})
	// Add the user Id in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then( course => {
		// Add the userId in the courses enrollees array
		course.enrollees.push({userId : data.userId});

		// Saves the updated course information in the database
		return course.save().then((course, err) => {
			if(err){
				return false;
			}else{
				return true;
			}
		})
	})

	// Condition that will if the user and course documents have been updated
	// User enrollment is successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	}
	// User enrollment failure
	else{
		return false;
	}
}



